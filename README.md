# UNPAR E-Vote (Proposal 1)

[![forthebadge](https://forthebadge.com/images/badges/built-with-grammas-recipe.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/does-not-contain-msg.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/gluten-free.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com)

E-Voting Central API. Proposal. Yep, masih proposal

## System Requirements

- PHP 7.3
  - OpenSSL
  - Composer
  - cUrl
- MySQL / Postgre (not-tested) / MsSQL (not-tested)

## Features
TBD

### API Docs
TBD, untuk sementara ke bagian [Proposal Document](https://gitlab.com/chez14/evote-kiriyama/wikis/home).

## Getting Started
- Invoke this:
    ```shell
    $ composer install
    ```
- Do your own setup on `app/config/`
  
  find for `.example.ini` file, and change them as your system setups.

- Run a migration
  
  You can access `http://localhost:8080/-/migrate` to start the migration.
  
## License
[GPLv3](LICENSE).