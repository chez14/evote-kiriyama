<?php
namespace Migration;
/**
 * Migration Example
 * Please read more documentation on https://github.com/chez14/f3-ilgar
 */
class DBSchema extends \Chez14\Ilgar\MigrationPacket {
    public function on_migrate(){
        // Do your things here!
        // All the F3 object were loaded, F3 routines executed,
        // this will just like you doing things in your controller file.
        
        $f3 = \F3::instance(); //get the $f3 from here.
        
        \Model\System\Admin::setup();
        \Model\System\Acl::setup();
        \Model\System\AclItem::setup();

        \Model\Pemilu\Periode::setup();
        \Model\Pemilu\Tipe::setup();
        \Model\Pemilu\Fakultas::setup();
        \Model\Pemilu\Jurusan::setup();
        \Model\Pemilu\Calon::setup();
        \Model\Pemilu\Pemilih::setup();
        \Model\Pemilu\VoteSession::setup();
    }

    public function on_failed(\Exception $e) {
        \Model\System\Admin::setdown();
        \Model\System\Acl::setdown();
        \Model\System\AclItem::setdown();

        \Model\Pemilu\Periode::setdown();
        \Model\Pemilu\Tipe::setdown();
        \Model\Pemilu\Fakultas::setdown();
        \Model\Pemilu\Jurusan::setdown();
        \Model\Pemilu\Calon::setdown();
        \Model\Pemilu\Pemilih::setdown();
        \Model\Pemilu\VoteSession::setdown();
    }
}