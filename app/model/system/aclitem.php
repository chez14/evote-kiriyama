<?php
namespace Model\System;

class AclItem extends \DB\Cortex {
    protected
    $fieldConf = array(
        'codename'=>[
            'type'=>\DB\SQL\Schema::DT_TEXT,
            'nullable' => false,
            'index' => false,
            'unique' => false,
        ],
        'permission'=>[
            'type'=>\DB\SQL\Schema::DT_INT,
            'nullable' => false,
            'index' => false,
            'unique' => false,
        ],
        'acl' => [
            'belongs-to-one' => '\Model\System\Acl'
        ],
    ),
    $db = 'DB',
    $table = 'system_acl_item';

    const
        NONE = 0,
        CREATE = 1,
        READ = 1 << 1,
        UPDATE = 1 << 2,
        DELETE = 1 << 3,
        ALL = self::CREATE | self::READ | self::UPDATE | self::DELETE;

    public function save() {
        if(!$this->created_on)
            $this->created_on = time();
        $this->updated_on = time();
        return parent::save();
    }
}