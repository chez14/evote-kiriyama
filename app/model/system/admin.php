<?php
namespace Model\System;

use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Signer\Keychain;
use Lcobucci\JWT\Signer\Rsa\Sha256;

class Admin extends \DB\Cortex {
    protected
    $fieldConf = array(
        "username"=>[
            'type'=>\DB\SQL\Schema::DT_TEXT
        ],
        "password"=>[
            'type'=>\DB\SQL\Schema::DT_TEXT
        ],
        "email"=>[
            'type'=>\DB\SQL\Schema::DT_TEXT
        ],
        "permission"=>[
            'belongs-to-one'=>'\Model\System\Acl'
        ],

        'deleted_on'=>[
            'type'=>\DB\SQL\Schema::DT_DATETIME,
            'nullable' => true,
            'index' => false,
            'unique' => false,
        ],
        'created_on'=>[
            'type'=>\DB\SQL\Schema::DT_DATETIME,
            'nullable' => true,
            'index' => false,
            'unique' => false,
        ],
        'updated_on'=>[
            'type'=>\DB\SQL\Schema::DT_DATETIME,
            'nullable' => true,
            'index' => false,
            'unique' => false,
        ],
    ),
    $db = 'DB',
    $table = 'admin';

    const
        E_GENERIC = "Kombinasi User dan Password tidak ditemukan, coba lagi, mungkin?",
        E_LOGIN_NOT_SUPPORTED = "Login untuk tipe user anda saat ini belum di support.";

    
    
    public function set_deleted_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function set_created_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function set_updated_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function save() {
        if(!$this->created_on)
            $this->created_on = time();
        $this->updated_on = time();
        return parent::save();
    }

    public static function getFromSession($statusOnly=false){
        $f3 = \Base::instance();
        if(!$f3->exists('SESSION.user'))
            return null;
        
        if($statusOnly) {
            return "user";
        }

        $user = new self;
        $user->load(['id = ?',$f3->get('SESSION.user')]);
        return $user;
    }

    public static function getFromHTTPHeader(){
        if(!\Base::instance()->exists('SERVER.HTTP_Authorization')) {
            return false;
        }
        $auth_header = \Base::instance()->get('SERVER.HTTP_Authorization');
        $auth_header = \substr($auth_header, strlen("Bearer "));
        $token = (new Parser())->parse($auth_header);

        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)
        $data->setIssuer(\Base::instance()->get('SECURITY.issuer'));
        

        $signer = new Sha256();
        $keychain = new Keychain();

        if(!$token->validate($data) || $token->verify(
            $signer, 
            $keychain->getPublicKey("file://" . \Base::instance()->get('SECURITY.publickey_path')))) {
            return false;
        }

        $user = new self();
        $user->load(['id=?', $token->getClaim('uid')]);
        if($user->dry()){
            return false;
        }
        return $user;
    }

    public function generateToken() {
        if($this->dry()){
            throw new \Exception('Model is dry!');
        }
        $signer = new Sha256();
        $keychain = new Keychain();
        $token = (new Builder())->setIssuer(\Base::instance()->get('SECURITY.issuer'))
            ->setIssuedAt(time())
            ->setExpiration(time() + \Base::instance()->get('SECURITY.expiration'))
            ->set('uid', $this->_id)
            ->sign($signer,  $keychain->getPrivateKey("file://" . \Base::instance()->get('SECURITY.privatekey_path')))
            ->getToken();
        return $token;
    }

    public static function login($username, $password, $set_session = false){
        $user = new self;
        $user->load(['username=?', $username]);
        if(!$user->loaded())
            throw new \Exception(self::E_GENERIC);

        if($user->password == null) {
            // implement login pake Radius / ADDS disini.
            throw new \Exception(self::E_LOGIN_NOT_SUPPORTED);
        }

        if(!$user->auth($password))
            throw new \Exception(self::E_GENERIC);
        
        $f3 = \Base::instance();

        if($set_session){
            $f3->set('SESSION.user', $user->id);
        }
        return $user;
    }

    public function auth($password){
        return password_verify($password, $this->password);
    }


    public function set_password($pass){
        return password_hash($pass, CRYPT_BLOWFISH);
    }

    public function cast ($obj = NULL, $rel_depths = 1, $save_cast = true) {
        $obj = parent::cast($obj, $rel_depths);
        if(!$save_cast) {
            return $obj;
        } else {
            unset($obj['password']);
            return $obj;
        }
    }
}
?>
