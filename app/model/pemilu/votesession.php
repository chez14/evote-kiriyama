<?php
namespace Model\Pemilu;

class VoteSession extends \DB\Cortex {
    protected
        $fieldConf = array(
            'pemilih' => [
                'belongs-to-one' => '\Model\Pemilu\Pemilih'
            ],
            'penerbit' => [
                'belongs-to-one' => '\Model\System\Admin'
            ],
            'claimed_on' => [
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'expired_on' => [
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'deleted_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'created_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => false,
                'index' => false,
                'unique' => false,
            ],
            'updated_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => false,
                'index' => false,
                'unique' => false,
            ],
        ),
    $db = 'DB',
    $table = 'vote_session';

    public function set_claimed_on($date) {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_expired_on($date) {
        return date("Y-m-d H:i:s", $date);
    }

    public function set_deleted_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function set_created_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function set_updated_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function save() {
        if(!$this->created_on)
            $this->created_on = time();
        $this->updated_on = time();
        return parent::save();
    }
}
