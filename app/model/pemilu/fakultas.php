<?php
namespace Model\Pemilu;

class Fakultas extends \DB\Cortex {
    protected
        $fieldConf = array(
            'name'=>[
                'type'=>\DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
            ],
            'jurusan' => [
                'has-many' => ['\Model\Pemilu\Jurusan', 'fakultas']
            ],
            'calon' => [
                'has-many' => ['\Model\Pemilu\Calon', 'fakultas']
            ]
        ),
    $db = 'DB',
    $table = 'fakultas';
}
