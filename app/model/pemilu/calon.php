<?php
namespace Model\Pemilu;

class Calon extends \DB\Cortex {
    protected
        $fieldConf = array(
            'name'=>[
                'type'=>\DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
            ],
            'tipe' => [
                'belongs-to-one' => '\Model\Pemilu\Tipe'
            ],
            'fakultas' => [
                'belongs-to-one' => '\Model\Pemilu\Fakultas'
            ],
            'jurusan' => [
                'belongs-to-one' => '\Model\Pemilu\Jurusan'
            ],
            'periode' => [
                'belongs-to-one' => '\Model\Pemilu\Periode'
            ],

            'deleted_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'created_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'updated_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
        ),
    $db = 'DB',
    $table = 'calon';

    public function set_deleted_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function set_created_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function set_updated_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function save() {
        if(!$this->created_on)
            $this->created_on = time();
        $this->updated_on = time();
        return parent::save();
    }
}
