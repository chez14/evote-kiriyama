<?php
namespace Model\Pemilu;

class Jurusan extends \DB\Cortex {
    protected
        $fieldConf = array(
            'name'=>[
                'type'=>\DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
            ],
            'jurusan' => [
                'has-many' => '\Model\Pemilu\Jurusan'
            ],
            'fakultas' => [
                'belongs-to-one' => '\Model\Pemilu\Fakultas'
            ],
            'calon' => [
                'has-many' => '\Model\Pemilu\Calon'
            ]
        ),
    $db = 'DB',
    $table = 'jurusan';
}
