<?php
namespace Model\Pemilu;

class Tipe extends \DB\Cortex {
    protected
        $fieldConf = array(
            'nama' => [
                'type'=>\DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
            ]
        ),
    $db = 'DB',
    $table = 'calon_tipe';
}
