<?php
namespace Model\Pemilu;

class Periode extends \DB\Cortex {
    protected
        $fieldConf = array(
            'name'=>[
                'type'=>\DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => false,
                'unique' => false,
            ],
            'currently_active' => [
                'type'=>\DB\SQL\Schema::DT_BOOL,
                'nullable' => false,
                'index' => false,
                'unique' => false,
                'default' => false,
            ],
            'deleted_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'created_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
            'updated_on'=>[
                'type'=>\DB\SQL\Schema::DT_DATETIME,
                'nullable' => true,
                'index' => false,
                'unique' => false,
            ],
        ),
    $db = 'DB',
    $table = 'periode';

    public function getActivePeriode() {
        $periode = self::find(["deleted_on = ? and currently_active = ?", null, true]);
        if(!$periode) {
            return [];
        } else {
            $periode[0];
        }
    }

    public function getLatestPeriode() {
        $periode = self::find(["deleted_on = ?", null], [
            "order" => "created_on DESC",
            "limit" => 1
        ]);
        if(!$periode) {
            return [];
        } else {
            $periode[0];
        }
    }

    public function set_deleted_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function set_created_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function set_updated_on($date) {
        return date("Y-m-d H:i:s", $date);
    }
    
    public function save() {
        if(!$this->created_on)
            $this->created_on = time();
        $this->updated_on = time();
        return parent::save();
    }
}
