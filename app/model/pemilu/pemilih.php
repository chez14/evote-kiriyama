<?php
namespace Model\Pemilu;

class Pemilih extends \DB\Cortex {
    protected
        $fieldConf = array(
            'npm'=>[
                'type'=>\DB\SQL\Schema::DT_TEXT,
                'nullable' => false,
                'index' => true,
                'unique' => true,
            ],
            'jurusan' => [
                'belongs-to-one' => '\Model\Pemilu\Jurusan'
            ],
        ),
    $db = 'DB',
    $table = 'pemilih';
}
