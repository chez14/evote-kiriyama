<?php

namespace Controller;

abstract class CRUDBase extends \Prefab
{

    protected $publicPermission = \Model\System\AclItem::NONE;
    protected $permissionPrefix = "";
    protected $model = null;

    public function beforeroute($f3)
    { }

    public function get_index($f3)
    { 
        $model = new $this->model;
        $models = $model->find();
        if($models === false) {
            $models = [];
        } else {
            $models = $models->castAll();
        }
        return \View\Api::success($models);
    }

    public function get_item($f3)
    { 
        $model = new $this->model;
        $model->load(["id = ?", $f3->PARAMS['id']]);

        return \View\Api::success($model->cast());
    }

    public function post_index($f3)
    { }

    public function put_item($f3)
    { }

    public function delete_item($f3)
    { }
}
