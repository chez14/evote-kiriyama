<?php
namespace Controller\Api;

use Controller\CRUDBase;

class Fakultas extends CRUDBase
{

    protected $model = '\Model\Pemilu\Fakultas';
    protected $permissionPrefix = "fakultas";
    
}
